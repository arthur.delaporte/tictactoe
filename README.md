# TicTacToe

TicTacToe est une application de jeu du Morpion (Tic-Tac-Toe) réalisée en HTML, CSS et JavaScript.

***

## Comment jouer

- Ouvrez le fichier `index.html` dans votre navigateur pour lancer le jeu.
- Le jeu se joue à deux joueurs, un joueur joue avec les "X" et l'autre avec les "O".
- Les joueurs alternent pour placer leurs symboles sur la grille en cliquant sur les cases vides.
- Le premier joueur qui réussit à aligner trois de ses symboles horizontalement, verticalement ou en diagonale remporte la partie.
- Si toutes les cases sont remplies sans qu'aucun joueur ne gagne, la partie est déclarée nulle.

***

## Installation

Pour utiliser l'application TicTacToe, suivez ces étapes :

1. Clonez le dépôt du projet sur votre ordinateur :

   ```shell
   git clone https://gitlab.com/arthur.delaporte/tictactoe.git
   ```

2. OUvrez le fichier `index.html` dans votre navigateur.

3. Jouez au TicTacToe avec un ami en utilisant le même ordinateur.

***

## Auteur

Ce projet a été réalisé par [Arthur Delaporte](https://gitlab.com/arthur.delaporte).
