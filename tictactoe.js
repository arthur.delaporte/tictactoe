// fonciton pour calculer chaque possibilité
function funcVictory() {

    var case1, case2, case3, case4, case5, case6, case7, case8, case9;
    case1 = document.getElementById("case1").value;
    case2 = document.getElementById("case2").value;
    case3 = document.getElementById("case3").value;
    case4 = document.getElementById("case4").value;
    case5 = document.getElementById("case5").value;
    case6 = document.getElementById("case6").value;
    case7 = document.getElementById("case7").value;
    case8 = document.getElementById("case8").value;
    case9 = document.getElementById("case9").value;


    if (case1 === 'X' && case2 === 'X' && case3 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case4").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case4 === 'X' && case5 === 'X' && case6 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case7 === 'X' && case8 === 'X' && case9 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case6").disabled = true;
    }

    else if (case1 === 'X' && case4 === 'X' && case7 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case2 === 'X' && case5 === 'X' && case8 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case1").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case3 === 'X' && case6 === 'X' && case9 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
    }

    else if (case1 === 'X' && case5 === 'X' && case9 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
    }

    else if (case3 === 'X' && case5 === 'X' && case7 === 'X') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des X";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }


    else if (case1 === 'O' && case2 === 'O' && case3 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case4").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case4 === 'O' && case5 === 'O' && case6 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case7 === 'O' && case8 === 'O' && case9 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case6").disabled = true;
    }

    else if (case1 === 'O' && case4 === 'O' && case7 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case2 === 'O' && case5 === 'O' && case8 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case1").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case9").disabled = true;
    }

    else if (case3 === 'O' && case6 === 'O' && case9 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case5").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
    }

    else if (case1 === 'O' && case5 === 'O' && case9 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case2").disabled = true;
        document.getElementById("case3").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case7").disabled = true;
        document.getElementById("case8").disabled = true;
    }

    else if (case3 === 'O' && case5 === 'O' && case7 === 'O') {
        document.getElementById('whoSTurn').innerHTML = "Victoire des O";
        document.getElementById("case1").disabled = true;
        document.getElementById("case2").disabled = true;
        document.getElementById("case4").disabled = true;
        document.getElementById("case6").disabled = true;
        document.getElementById("case8").disabled = true;
        document.getElementById("case9").disabled = true;
    }


    else if (case1 != "" && case2 != "" && case3 != "" && case4 != "" && case5 != "" && case6 != "" && case7 != "" && case8 != "" && case9 != "") {
            document.getElementById('whoSTurn').innerHTML = "Match Nul";
    }

    else {

        if (turn === 1) {
            document.getElementById('whoSTurn').innerHTML = "Tour des X";
        }
        else {
            document.getElementById('whoSTurn').innerHTML = "Tour des 0";
        }
    }
}

//fonction pour remettre toutes les cases vides au RESET
function funcReset() {
    location.reload();
    document.getElementById("case1").value = '';
    document.getElementById("case2").value = '';
    document.getElementById("case3").value = '';
    document.getElementById("case4").value = '';
    document.getElementById("case5").value = '';
    document.getElementById("case6").value = '';
    document.getElementById("case7").value = '';
    document.getElementById("case8").value = '';
    document.getElementById("case9").value = '';
 
}

//fonction pour changer la couleur du background
function changeBackground(color) {
   document.body.style.backgroundColor = color;
}

//on commence avec le tour 1
//tour 1 = tour X
//tour 0 = tour O
turn = 1;

//fonction lorsqu'on clique sur la case 1
function funcCase1() {
    if (turn === 1) {
        document.getElementById("case1").style.color = "blue";
        document.getElementById("case1").value = "X";
        document.getElementById("case1").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case1").style.color = "red";
        document.getElementById("case1").value = "O";
        document.getElementById("case1").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 2
function funcCase2() {
    if (turn === 1) {
        document.getElementById("case2").style.color = "blue";
        document.getElementById("case2").value = "X";
        document.getElementById("case2").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case2").style.color = "red";
        document.getElementById("case2").value = "O";
        document.getElementById("case2").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 3
function funcCase3() {
    if (turn === 1) {
        document.getElementById("case3").style.color = "blue";
        document.getElementById("case3").value = "X";
        document.getElementById("case3").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case3").style.color = "red";
        document.getElementById("case3").value = "O";
        document.getElementById("case3").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 4
function funcCase4() {
    if (turn === 1) {
        document.getElementById("case4").style.color = "blue";
        document.getElementById("case4").value = "X";
        document.getElementById("case4").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case4").style.color = "red";
        document.getElementById("case4").value = "O";
        document.getElementById("case4").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 5
function funcCase5() {
    if (turn === 1) {
        document.getElementById("case5").style.color = "blue";
        document.getElementById("case5").value = "X";
        document.getElementById("case5").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case5").style.color = "red";
        document.getElementById("case5").value = "O";
        document.getElementById("case5").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 6
function funcCase6() {
    if (turn === 1) {
        document.getElementById("case6").style.color = "blue";
        document.getElementById("case6").value = "X";
        document.getElementById("case6").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case6").style.color = "red";
        document.getElementById("case6").value = "O";
        document.getElementById("case6").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 7
function funcCase7() {
    if (turn === 1) {
        document.getElementById("case7").style.color = "blue";
        document.getElementById("case7").value = "X";
        document.getElementById("case7").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case7").style.color = "red";
        document.getElementById("case7").value = "O";
        document.getElementById("case7").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 8
function funcCase8() {
    if (turn === 1) {
        document.getElementById("case8").style.color = "blue";
        document.getElementById("case8").value = "X";
        document.getElementById("case8").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case8").style.color = "red";
        document.getElementById("case8").value = "O";
        document.getElementById("case8").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}

//fonction lorsqu'on clique sur la case 9
function funcCase9() {
    if (turn === 1) {
        document.getElementById("case9").style.color = "blue";
        document.getElementById("case9").value = "X";
        document.getElementById("case9").disabled = true;
        changeBackground('red');
        document.getElementById("buttonReset").style.color = "red";
        turn = 0;
    }
    else {
        document.getElementById("case9").style.color = "red";
        document.getElementById("case9").value = "O";
        document.getElementById("case9").disabled = true;
        changeBackground('blue');
        document.getElementById("buttonReset").style.color = "blue";
        turn = 1;
    }
}